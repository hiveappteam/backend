# README
Dummy Flask backend container for hiveapp

## build
```
$ ./scripts/build.sh
```
## запуск без контейнера
```
$ python3 -m venv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
$ scripts/local.sh
```
## проверка работы
```
$ source venv/bin/activate
$ http get localhost:5005/api/v1/tags
```
## схема
* [validators.py](hiveapp/validators.py)
## routes
* *api/v1/tags* GET --> []
список отсортирован по *upvotes desc*, *downvotes asc*
* *api/v1/tags* POST --> {}
* *api/v1/tags/up/<id>* PUT --> {}
* *api/v1/tags/down/<id>* PUT --> {}
