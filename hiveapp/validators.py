from jsonschema import validate, ValidationError


tag_schema = {
    'type': 'object',
    'properties': {
        'changed_at': {'type': 'string'},
        'created_at': {'type': 'string'},
        'id': {'type': 'number'},
        'upvotes': {'type': 'number'},
        'downvotes': {'type': 'number'},
        'name': {'type': 'string'},
        'properties': {
            'anyOf': [
                {'type': 'object'},
                {'type': 'null'}
            ]
        },
        'url': {'type': 'string'}
    },
    'required': ['name']
}


def process(data, schema):
    try:
        validate(data, schema)
    except ValidationError as e:
        message = {}
        message['error'] = {}
        message['error']['message'] = e.message
        message['error']['instance'] = e.instance
        if len(e.path) > 0:
            message['error']['path'] = str(e.path[0])
        return message
    return None


def validate_tag(request):
    return process(request.get_json(), tag_schema)
