from hiveapp.models.tag import Tag
from hiveapp.validators import validate_tag
from flask import Blueprint, jsonify, request
from flask_cors import cross_origin
import logging
logging.basicConfig(
    format='[%(asctime)s] [%(levelname)s] %(message)s',
    level=logging.INFO)


tags = Blueprint('tags', __name__)


@tags.route('', methods=['GET'])
def tags_get():
    logging.warning('/tags GET {}'.format(request.args))
    try:
        lst = Tag.query.order_by(Tag.upvotes.desc()).order_by(Tag.downvotes.asc()).limit(100).all()
        return jsonify(lst), 200
    except BaseException as inst:
        logging.error(inst)
        return jsonify({'error': 'internal'}), 500


@tags.route('/down/<id>', methods=['PUT'])
def tags_down_put(id):
    logging.info(f"/tags/down/{id} PUT")
    try:
        data = {'id': id, 'downvotes': True}
        obj = Tag.query.get(id).update(data)
        return jsonify(obj), 200
    except BaseException as inst:
        logging.error(inst)
        return jsonify({'error': 'internal'}), 500


@tags.route('/up/<id>', methods=['PUT'])
def tags_up_put(id):
    logging.info(f"/tags/up/{id} PUT")
    try:
        data = {'id': id, 'upvotes': True}
        obj = Tag.query.get(id).update(data)
        return jsonify(obj), 200
    except BaseException as inst:
        logging.error(inst)
        return jsonify({'error': 'internal'}), 500


@tags.route('', methods=['POST'])
def tags_post():
    logging.info(f"/tags POST {request.get_json()}")
    try:
        validate = validate_tag(request)
        if validate is None:
            obj = Tag().add(request.get_json())
            return jsonify(obj), 200
        return jsonify(validate)
    except BaseException as inst:
        logging.error(inst)
        return jsonify({'error': 'internal'}), 500
