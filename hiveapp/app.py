import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager
from flask_cors import CORS

from hiveapp.encoder import HiveBaseEncoder


app = Flask(__name__)
app.config['SECRET_KEY'] = os.environ.get('SECRET', 'SECRET')
app.config['JWT_SECRET_KEY'] = os.environ.get('SECRET', 'SECRET')
app.config['JWT_TOKEN_LOCATION'] = ['cookies', 'headers']
app.config['JWT_ACCESS_COOKIE_PATH'] = '/api/v1'
app.config['JWT_COOKIE_CSRF_PROTECT'] = False
app.config['JWT_COOKIE_SECURE'] = True
app.config['JWT_COOKIE_SAMESITE'] = None
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('HIVEAPP_PSQL_URI', 'postgresql://postgres@localhost:5432/hivebase')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.json_encoder = HiveBaseEncoder
jwt = JWTManager(app)
orm = SQLAlchemy(app)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
