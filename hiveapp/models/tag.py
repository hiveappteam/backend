from hiveapp.app import orm
from sqlalchemy.dialects.postgresql import TIMESTAMP, JSON
from sqlalchemy import func


class Tag(orm.Model):
    __tablename__ = 'hive_tag'
    id = orm.Column(orm.Integer, primary_key=True)
    name = orm.Column(orm.String(0), nullable=False, unique=True)
    created_at = orm.Column(TIMESTAMP, nullable=False, server_default='CURRENT_TIMESTAMP')
    changed_at = orm.Column(TIMESTAMP, nullable=False, server_default='CURRENT_TIMESTAMP')
    upvotes = orm.Column(orm.Integer, default=1)
    downvotes = orm.Column(orm.Integer, default=0)
    url = orm.Column(orm.String(0))
    properties = orm.Column(JSON)
    removed = orm.Column(orm.Boolean)

    def add(self, data):
        lst = ['name', 'url', 'properties']
        for i in data:
            if i not in lst:
                data[i] = None
        obj = Tag(**data)
        orm.session.add(obj)
        orm.session.commit()
        return obj

    def update(self, data):
        if 'upvotes' in data:
            self.upvotes += 1
        if 'downvotes' in data:
            self.downvotes += 1
        self.changed_at = func.now()
        orm.session.commit()
        return self

    def dict(self):
        return {
            'changed_at': self.changed_at.timestamp(),
            'created_at': self.created_at.timestamp(),
            'downvotes': self.downvotes,
            'id': self.id,
            'name': self.name,
            'properties': self.properties,
            'upvotes': self.upvotes,
            'url': self.url
        }
