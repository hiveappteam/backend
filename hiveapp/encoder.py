import json
import datetime

class HiveBaseEncoder(json.JSONEncoder):

    def default(self, obj):
        from hiveapp.models.tag import Tag
        if isinstance(obj, Tag):
            return obj.dict()
        if isinstance(obj, (datetime.date, datetime.datetime)):
            return obj.isoformat()
        
        return super().default(obj)