#!/bin/bash

if [ -z "$HIVEAPP_REQUEST_TIMEOUT"]; then
    HIVEAPP_REQUEST_TIMEOUT=10
fi
if [ -z "$HIVEAPP_API_PORT"]; then
    HIVEAPP_API_PORT=5005
fi
if [ -z "$HIVEAPP_NUM_WORKERS"]; then
    HIVEAPP_NUM_WORKERS=1
fi

. venv/bin/activate

gunicorn -t ${HIVEAPP_REQUEST_TIMEOUT} -w ${HIVEAPP_NUM_WORKERS} -b 0.0.0.0:${HIVEAPP_API_PORT} hiveapp.app:app
