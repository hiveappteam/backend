FROM python:3.7-slim

WORKDIR app

COPY requirements.txt ./
RUN python -m venv venv
RUN venv/bin/pip install --upgrade pip
RUN venv/bin/pip install -r requirements.txt

COPY hiveapp hiveapp
COPY setup.py ./
RUN venv/bin/pip install -e .

COPY gunicorn.sh ./
CMD ["./gunicorn.sh"]
